class User < ApplicationRecord
	has_many :microposts
	validates :name, presence: true    # name can't be blank
    validates :email, presence: true    # email can't be blank
end
